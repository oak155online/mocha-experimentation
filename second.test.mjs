'use strict';

import { Browser, Builder} from 'selenium-webdriver';
import { WebDriverBot } from './test/webdriverbot.mjs';
import { BBCHomepage } from './test/bbc-homepage.mjs';
import { BBCSearchPage } from './test/bbc-search-page.mjs';

(async function example() {
  const bot = new WebDriverBot(await new Builder().forBrowser(Browser.CHROME).build());
  const bbcHomepage = new BBCHomepage(bot);
  const bbcSearchPage = new BBCSearchPage(bot);

  try {
    await bot.maximizeBrowserWindow();
    await bot.navigateTo(BBCHomepage.uri);
    await bbcHomepage.goToSiteSearch();
    await bbcSearchPage.searchFor('afro');
  } finally {
    await bot.driver.quit();
  }
})();
